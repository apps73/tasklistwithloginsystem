package com.teste.tasklist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

public class ActivityLogin extends AppCompatActivity {
    private Button btnGoToRegister;
    private EditText editUsernameLogin;
    private EditText editPasswordLogin;
    private Button btnConfirmLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        verifyAuthSession();
        btnGoToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityRegister.class);
                startActivity(intent);
            }
        });

        btnConfirmLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginUser();
            }
        });
    }

    private void loginUser(){
        if(!editUsernameLogin.getText().toString().equals("") && !editPasswordLogin.getText().toString().equals("") ){
            ParseUser.logInInBackground(editUsernameLogin.getText().toString(), editPasswordLogin.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if(e == null){
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }else{
            Toast.makeText(getApplicationContext(), "Informe Login E Senha!", Toast.LENGTH_LONG).show();
        }
    }

    private void verifyAuthSession(){
        ParseUser user = ParseUser.getCurrentUser();
        if(user != null){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void init(){
        btnGoToRegister = findViewById(R.id.btnGoToRegister);
        editUsernameLogin = findViewById(R.id.editUsernameLogin);
        editPasswordLogin = findViewById(R.id.editPasswordLogin);
        btnConfirmLogin = findViewById(R.id.btnConfirmLogin);
    }
}
