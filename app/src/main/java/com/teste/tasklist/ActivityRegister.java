package com.teste.tasklist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class ActivityRegister extends AppCompatActivity {
    private Button btnGoToLogin;
    private EditText usernameEditRegister;
    private EditText emailEditRegister;
    private EditText passwordEditRegister;
    private EditText confirmPasswordEditRegister;
    private Button btnConfirmRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();

        btnGoToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                startActivity(intent);
            }
        });

        btnConfirmRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });

    }

    public void init(){
        btnGoToLogin = findViewById(R.id.btnGoToLogin);
        usernameEditRegister = findViewById(R.id.usernameEditRegister);
        emailEditRegister = findViewById(R.id.emailEditRegister);
        passwordEditRegister = findViewById(R.id.passwordEditRegister);
        confirmPasswordEditRegister = findViewById(R.id.confirmPasswordEditRegister);
        btnConfirmRegistration = findViewById(R.id.btnConfirmRegistration);
    }

    public void registerUser(){
        if(!usernameEditRegister.getText().toString().equals("") &&
                !emailEditRegister.getText().toString().equals("") &&
                !passwordEditRegister.getText().toString().equals("") &&
                !confirmPasswordEditRegister.getText().toString().equals("")
        ){
            if(passwordEditRegister.getText().toString().equals(confirmPasswordEditRegister.getText().toString())){
                ParseUser user = new ParseUser();
                user.setUsername(usernameEditRegister.getText().toString());
                user.setEmail(emailEditRegister.getText().toString());
                user.setPassword(passwordEditRegister.getText().toString());
                user.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e == null){
                            Toast.makeText(getApplicationContext(), "Cadastro Realizado Com Sucesso!", Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                            startActivity(intent);
                        }else{
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }else{
                Toast.makeText(getApplicationContext(), "Senha Não Conferem", Toast.LENGTH_LONG).show();
                passwordEditRegister.setInputType(1);
                confirmPasswordEditRegister.setInputType(1);
            }
        }else{
            Toast.makeText(getApplicationContext(), "Um ou mais campos estão vazios!", Toast.LENGTH_LONG).show();
        }
    }
}
