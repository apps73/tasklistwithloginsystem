package com.teste.tasklist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbarMain;
    private ListView listView;
    AdapterTaskList adapterTask;
    private ArrayList<ParseObject> arrayList;
    private ParseQuery<ParseObject> query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        toolbarMain.setTitle("Lista De Tarefas");
        toolbarMain.inflateMenu(R.menu.menu_main);
        toolbarMain.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.actionLogout:
                        ParseUser.logOut();
                        Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                        startActivity(intent);
                        finish();
                        break;
                    case R.id.actionAddTask:
                        Intent intent1 = new Intent(getApplicationContext(), ActivityAddTask.class);
                        startActivity(intent1);
                        break;
                }
                return true;
            }
        });

        adapterTask = new AdapterTaskList(getApplicationContext(), arrayList);
        listView.setAdapter(adapterTask);
        getTask();
    }

    public void init(){
        toolbarMain = findViewById(R.id.toolbarMain);
        listView = findViewById(R.id.listView);
        arrayList = new ArrayList<>();
    }

    public void getTask(){
        query = ParseQuery.getQuery("Task");
        query.whereEqualTo("username", ParseUser.getCurrentUser().getUsername());
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(objects.size() > 0 && e == null){
                    arrayList.clear();
                    for (ParseObject parseObject : objects){
                        arrayList.add(parseObject);
                    }
                    adapterTask.notifyDataSetChanged();
                }else{
                    arrayList.clear();
                    if (e != null) Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    else Toast.makeText(MainActivity.this, "Não há itens cadastrados!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
