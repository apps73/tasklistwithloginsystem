package com.teste.tasklist;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

public class AdapterTaskList extends ArrayAdapter<ParseObject> {

    private Context context;
    private ArrayList<ParseObject> list;

    public AdapterTaskList(Context context, ArrayList<ParseObject> objects) {
        super(context, 0, objects);
        this.context = context;
        this.list = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if(view == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.activity_task, parent, false);
        }
        if(this.list.size() > 0){
            TextView showTask = view.findViewById(R.id.showTask);
            TextView showDateAndHour = view.findViewById(R.id.showDateAndHour);
            ImageView btnRemoveTask = view.findViewById(R.id.btnRemoveTask);

            final ParseObject parseObject = list.get(position);
            showTask.setText(parseObject.getString("Title"));
            showDateAndHour.setText(parseObject.getString("hour")+" - "+parseObject.getString("date"));

            btnRemoveTask.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ParseQuery<ParseObject> query  = ParseQuery.getQuery("Task");
                    query.whereEqualTo("objectId", parseObject.getObjectId());
                    query.findInBackground(new FindCallback<ParseObject>() {
                        @Override
                        public void done(List<ParseObject> objects, ParseException e) {
                            if(objects.size() > 0 && e == null){
                                for(ParseObject object : objects){
                                    try {
                                        object.delete();
                                        remove(parseObject);
                                        notifyDataSetChanged();
                                    } catch (ParseException ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            }
                        }
                    });
                }
            });
        }
        return view;
    }
}
