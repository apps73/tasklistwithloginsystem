package com.teste.tasklist;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseSession;
import com.parse.ParseUser;
import com.parse.SaveCallback;

public class ActivityAddTask extends AppCompatActivity {

    private Toolbar toolbarAddTask;
    private EditText editTask;
    private EditText editDate;
    private EditText editHour;
    private Button btnSaveTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);
        init();

        //MaskedFormatter formatter = new MaskedFormatter("##/##/####");
        //editDate.addTextChangedListener(new MaskedWatcher(formatter, editDate));

        toolbarAddTask.setTitle("Add Tarefa");
        toolbarAddTask.setNavigationIcon(getResources().getDrawable(R.drawable.ic_chevron_left));
        toolbarAddTask.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

        btnSaveTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTask();
            }
        });
    }

    public void saveTask(){
        if(!editTask.getText().toString().equals("") &&
                !editDate.getText().toString().equals("") &&
                !editHour.getText().toString().equals("")
        ){
            ParseObject object = new ParseObject("Task");
            object.put("username", ParseUser.getCurrentUser().getUsername());
            object.put("Title", editTask.getText().toString());
            object.put("date", editDate.getText().toString());
            object.put("hour", editHour.getText().toString());
            object.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if(e == null){
                        Toast.makeText(ActivityAddTask.this, "Tarefa Salva Com Sucesso!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(ActivityAddTask.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void init(){
        toolbarAddTask = findViewById(R.id.toolbarAddTask);
        editTask = findViewById(R.id.editTask);
        editDate = findViewById(R.id.editDate);
        editHour = findViewById(R.id.editHour);
        btnSaveTask = findViewById(R.id.btnSaveTask);
    }
}
